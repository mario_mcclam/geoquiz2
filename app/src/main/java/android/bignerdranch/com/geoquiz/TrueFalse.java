package android.bignerdranch.com.geoquiz;

/**
 * Created by coolm_000 on 12/30/2014.
 */
public class TrueFalse {
    private int mQuestion;
    private boolean mTrueQuestion;

   public TrueFalse(int question, boolean trueQuestion){
       mQuestion=question;
       mTrueQuestion=trueQuestion;
   }

    public int getQuestion() {
        return mQuestion;
    }

    public void setQuestion(int Question) {
        mQuestion = Question;
    }

    public boolean isTrueQuestion() {
        return mTrueQuestion;
    }

    public void setTrueQuestion(boolean trueQuestion) {
        mTrueQuestion = trueQuestion;
    }
}
