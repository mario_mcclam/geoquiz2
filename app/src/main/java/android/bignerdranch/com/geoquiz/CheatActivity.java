package android.bignerdranch.com.geoquiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by coolm_000 on 1/18/2015.
 */
public class CheatActivity extends Activity {
    private static final String KEY_IS_CHEATER = "IS_CHEATER";
    private static final String KEY_ANSWER_IS_TRUE = "ANSWER_IS_TRUE";
    public static final String EXTRA_ANSWER_IS_TRUE = "com.bignerdranch.android.geoquiz.ANSWER_IS_TRUE";
    public static final String EXTRA_ANSWER_SHOWN = "com.bignerdranch.android.geoquiz.ANSWER_SHOWN";
    public static final String  TAG="CheatActivity";
    boolean mAnswerIsTrue;
    boolean mIsCheater;

    TextView mAnswerTextView;
    Button mShowAnswer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);

        mAnswerTextView = (TextView) findViewById(R.id.answerTextView);

        mShowAnswer = (Button) findViewById(R.id.showAnswerButton);
        mShowAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAnswer();
                setAnswerShownResult(mIsCheater = true);
            }
        });

        if (savedInstanceState != null) {
            mAnswerIsTrue = savedInstanceState.getBoolean(KEY_ANSWER_IS_TRUE, false);
            mIsCheater = savedInstanceState.getBoolean(KEY_IS_CHEATER, false);
        } else {
            mAnswerIsTrue = getIntent().getBooleanExtra(EXTRA_ANSWER_IS_TRUE, false);
            mIsCheater = false;
        }
Log.d(TAG,"value of mIsCheater"+mIsCheater);
        if (mIsCheater) { setAnswer(); }

        setAnswerShownResult(mIsCheater);
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBoolean(KEY_IS_CHEATER, mIsCheater);
        savedInstanceState.putBoolean(KEY_ANSWER_IS_TRUE, mAnswerIsTrue);
    }

    private void setAnswerShownResult(boolean isAnswerShown) {
        Intent data = new Intent();
        data.putExtra(EXTRA_ANSWER_SHOWN, isAnswerShown);
        setResult(RESULT_OK, data);
    }

    private void setAnswer() {
        if (mAnswerIsTrue) {
            mAnswerTextView.setText(R.string.true_button);
        } else {
            mAnswerTextView.setText(R.string.false_button);
        }
    }
}