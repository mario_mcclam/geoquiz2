package android.bignerdranch.com.geoquiz;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


public class QuizActivity extends ActionBarActivity {
// Variables

    private Button mTrueButton;
    private Button mFalseButton;
    private ImageButton mNextButton;
    private ImageButton mPreviousButton;
    private Button mCheatButton;

    private Boolean mIsCheater=false;
    private TextView mQuestionTextView;
    private TextView mApiLevel;

    private static final String KEY_IS_CHEATER = "IS_CHEATER";
    private static final String TAG="QuizActivity";
    private static final String KEY_INDEX="index";

    private TrueFalse[] mQuestionBank= new TrueFalse[]{
      new TrueFalse(R.string.question_oceans,true),
      new TrueFalse(R.string.question_mideast,false),
      new TrueFalse(R.string.question_africa,false),
      new TrueFalse(R.string.question_americas,true),
      new TrueFalse(R.string.question_asia,true),
    };
    private int[] mCheaterArray= new int[4];
    private int mCurrentIndex=0;
// is cheater results
    @Override
    protected void  onActivityResult(int requestCode, int resultCode, Intent data){
        if (data == null){
            return;
        }
        mIsCheater=data.getBooleanExtra(CheatActivity.EXTRA_ANSWER_SHOWN,false);

        if (mIsCheater==true){
            int n;
            n=(mCurrentIndex)%mQuestionBank.length;
            mCheaterArray[n]=n;
        }

    }


// update question

    private void updateQuestion(){
        int question=mQuestionBank[mCurrentIndex].getQuestion();
        mQuestionTextView.setText(question);
    }

// Check Answer

    private void checkAnswer (boolean userPressedTrue) {
        boolean answerIsTrue = mQuestionBank[mCurrentIndex].isTrueQuestion();
        int messageResId = 0;
   if (mIsCheater) {
         messageResId = R.string.judgment_toast;
       } else {
            if (userPressedTrue == answerIsTrue) {
                messageResId = R.string.correct_toast;
            } else {
                messageResId = R.string.incorrect_toast;
            }
   }
            Toast.makeText(this, messageResId, Toast.LENGTH_SHORT).show();

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
// show API level
        mApiLevel= (TextView)findViewById(R.id.api_level);
        mApiLevel.setText("API Level "+Build.VERSION.SDK_INT);


// question
        mQuestionTextView=(TextView)findViewById(R.id.question_text_view);

        mQuestionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentIndex=(mCurrentIndex+1)% mQuestionBank.length;
                updateQuestion();

            }
        });
//
// True Button
        mTrueButton=(Button)findViewById(R.id.true_button);
        mTrueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            checkAnswer(true);
            }
        });

// False Button
        mFalseButton=(Button)findViewById(R.id.false_button);
        mFalseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(false);
            }
        });

//Next Button
        mNextButton=(ImageButton) findViewById(R.id.next_button);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentIndex <4) {
                    mCurrentIndex = (mCurrentIndex + 1) % mQuestionBank.length;
                } else {
                    Toast.makeText(QuizActivity.this, R.string.end, Toast.LENGTH_SHORT).show();
                }
                if (mCurrentIndex !=mCheaterArray[mCurrentIndex]) {

                    mIsCheater = false;
                }else {
                    mIsCheater=true;

                }
                    updateQuestion();
                }

            });
// Previous Button
        mPreviousButton=(ImageButton) findViewById(R.id.prev_button);
        mPreviousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentIndex > 0) {
                    mCurrentIndex = (mCurrentIndex - 1) % mQuestionBank.length;
                    updateQuestion();
                } else {
                    Toast.makeText(QuizActivity.this, R.string.no_more, Toast.LENGTH_SHORT).show();
                }

                if (mCurrentIndex!=mCheaterArray[mCurrentIndex]){

                    mIsCheater = false;
                }else{
                    mIsCheater = true;
                }
            }
        });

//Cheat Button
        mCheatButton=(Button)findViewById(R.id.cheat_button);
        mCheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(QuizActivity.this,CheatActivity.class);
                boolean answerIsTrue=mQuestionBank[mCurrentIndex].isTrueQuestion();
                i.putExtra(CheatActivity.EXTRA_ANSWER_IS_TRUE,answerIsTrue);
                startActivityForResult(i,0);
            }
        });
        if (savedInstanceState !=null){
            mCurrentIndex=savedInstanceState.getInt(KEY_INDEX,0);
            mIsCheater=savedInstanceState.getBoolean(KEY_IS_CHEATER,false);
        }

updateQuestion();

    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Log.i(TAG,"onSaveInstanceState");
        savedInstanceState.putInt(KEY_INDEX,mCurrentIndex);
        savedInstanceState.putBoolean(KEY_IS_CHEATER,mIsCheater);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quiz, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
